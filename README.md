# book-scraper

## usage
Install python 3.11+ or atleast 3.8+ (not tested but should work)
```bash
pip install -r requirements.txt
```
run `bookv3.py`, paste in link

if you're using it on your phone, you only need to copy `bookv3.py` there.
take a look inside of `requirements.txt` and install them.

this script is only for archiving or personal use, not for re-distributing others' works under your name

## supported sites
- RoyalRoad
- ScribbleHub

## features
- All requests are cached for 10 minutes
  - repeated downloading of same books in that timeframe will be a lot quicker.
- Proper EPUB metadata (Title, author, table of contents)
- Preserve formatting of paragraphs
- RoyalRoad: removes Anti-download warnings

:3