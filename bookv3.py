import argparse
import os
import re
import time

import cssutils
import requests_cache
from bs4 import BeautifulSoup
from ebooklib import epub
from random import choice

RR_HOSTNAME = "https://www.royalroad.com"
SH_HOSTNAME = "https://www.scribblehub.com"
USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.3"

# fallback user-agents to pick from. see: https://www.useragents.me updated 2024-08-09
useragents = [
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.3",
	"Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Mobile Safari/537.3",
	"Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Mobile/15E148 Safari/604.",
	"Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/25.0 Chrome/121.0.0.0 Mobile Safari/537.3",
	"Mozilla/5.0 (iPhone; CPU iPhone OS 17_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) GSA/323.0.647062479 Mobile/15E148 Safari/604.",
	"Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Mobile Safari/537.3"
]

def make_headers(referrer = None):
	head = { "User-Agent": USERAGENT }
	if referrer:
		head["referrer"] = referrer
	return head

headers = make_headers()
hostname = RR_HOSTNAME

# i would have liked to split this into multiple files but for this use-case (phone python) it's better in 1 file

# cache requests for 10 minutes to mitigate scribblehub rate-limiting
req = requests_cache.CachedSession("book_cache", expire_after=600) 

parser = argparse.ArgumentParser(description="Process some files.")
parser.add_argument("-o", "--output_folder", default="", help="Output folder path")
args = parser.parse_args()

print("example: https://www.royalroad.com/fiction/25699/magitech-awakenings")
print("example: https://www.scribblehub.com/series/858447/library-of-rain/")
book_url = input("input url to book (with all chapters): ")

book = epub.EpubBook()
book.set_language("en")

toc_list = []
chapter_obj_list = []

def ebook_add_chapter(chapter_name, chapter_filename, text):
	epub_chapter = epub.EpubHtml(title=chapter_name, file_name=chapter_filename)
	epub_chapter.set_content(text)
	chapter_obj_list.append(epub_chapter)
	book.add_item(epub_chapter)

	toc_list.append(epub.Link(chapter_filename, chapter_name, chapter_name))

def get_sanizated_string(dirty_string):
		dirty_string = re.sub(r'[\\/:*?"<>|;]', "_", dirty_string)
		return dirty_string.strip()

class RoyalRoad:
	def __init__(self, url: str):
		self.url = url
		res = req.get(self.url, headers=headers).content
		print("got main page from royalroad")
		
		soup = BeautifulSoup(res, "html.parser")
		self.book_title = soup.select_one(".fic-title h1").get_text()
		self.author = soup.select_one(".fic-title h4 a").get_text() + " - RoyalRoad"
		self.hostname = RR_HOSTNAME
	
	def __download_ch(self, chapter_url: str):
		print(chapter_url)
		html = req.get(chapter_url, headers=headers).content
		soup = BeautifulSoup(html, "html.parser")

		# royalroad injects a bunch of spans which say something like "this story has been stolen from Royal Road"
		# this removes them, since this script is only for archiving or personal use, not for re-distributing others' works under your name
		style_tags = soup.find_all("style")
		for tag in style_tags:
			sheet = cssutils.css.CSSStyleSheet()
			sheet.cssText = tag.get_text()

			# we find all all css rules which set display: none on a class, then remove the hidden elements from the bs4 tree
			for rule in sheet:
				found = False
				for prop in rule.style:
					if prop.name == "display" and prop.value == "none":
						found = True
						break
				if found:
					rmelem = soup.select_one(rule.selectorText)
					if rmelem is not None:
						rmelem.decompose()
		
		chapter_elems = soup.find_all("div", {"class":"chapter-inner chapter-content"})
		contents_list = []

		for elem in chapter_elems:
			contents_list.append(elem.encode_contents())
		text = b"\n".join(contents_list)

		chapter_name = soup.select_one(".fic-header h1").get_text()  #chapter_url.split("/")[-1]
		chapter_filename = f"chap_{self.counter}.xhtml"
		return chapter_name, chapter_filename, text
	
	def download_all(self):
		book.set_title(self.book_title)
		book.add_author(self.author)

		soup = BeautifulSoup(req.get(self.url).content, "html.parser")
		chapter_wrapper = soup.find("table", { "id": "chapters" })
		links = [ link["href"] for link in chapter_wrapper.find_all("a") ]
		links_deduped = list(dict.fromkeys(links))
		links_with_hostname = [self.hostname + link for link in links_deduped]

		self.counter = 1
		for link in links_with_hostname:
			ebook_add_chapter(*self.__download_ch(link))
			self.counter += 1

class ScribbleHub:
	def __init__(self, url: str):
		self.url = url
		if "/chapter/" in url:
			print("Incorrect ScribbleHub url - please paste a /series/ link, not /chapter/!")
			quit()
		
		self.book_title = ""
		self.author = ""
		self.first_ch: str = self.__get_first()
		self.counter = 1
		self.retries = 0
		self.max_retries = 10


	def __get_first(self):
		print("getting main page from scribblehub, please wait... (restart if this takes longer than 25s)")
		print(f"> {self.url}")
		print(headers)
		res = req.get(self.url, headers=headers).content
		print("got main page from scribblehub")
		

		soup = BeautifulSoup(res, "html.parser")
		reada = soup.select_one(".read_buttons a")["href"]

		self.book_title = soup.select_one(".fic_title").text
		self.author = soup.select_one(".auth_name_fic").text + " - ScribbleHub"
		return str(reada)
	
	def __next_ch(self): 
		global USERAGENT
		output = f"{self.counter} {self.first_ch}"
		print(output, end="\r")
		head = make_headers(self.first_ch)
		res = req.get(self.first_ch, headers=head).content
		soup = BeautifulSoup(res, "html.parser")
		
		self.retries = 0
		# once in a while scribblehub will return a Please wait... page with a body.no-js, which means we gotta wait & retry
		while soup.select_one('body.no-js') is not None and self.retries < self.max_retries:
			USERAGENT = choice(useragents)
			head = make_headers(self.first_ch)
			self.retries += 1
			print('\r' + ' ' * len(output), end='\r')
			output = f"{self.counter}: Got bot challenge, changing user-agent and retrying in {self.retries} seconds..."
			print(output, end="\r")
			time.sleep(self.retries)

			res = req.get(self.first_ch, headers=head).content
			soup = BeautifulSoup(res, "html.parser")
		if self.retries > 0:
			print('\r' + ' ' * len(output), end='\r')
			print(f"{self.counter} {self.first_ch} done after {self.retries} retries.", end="\n")
		else:
			print(end="\n")

		try:
			chapter_name = soup.select_one(".chapter-title").text
		except:
			print(f"chapter {self.first_ch} failed after {self.max_retries} retries! dumped html into debug.html")
			f = open('debug.html', "w", encoding="utf8")
			f.write(res.decode())
			f.close()
			
		chapter_filename = f"chap_{self.counter}.xhtml"
		raw_chapter = soup.find("div", { "id": "chp_raw" })
		text = "\n".join([f"<p>{p.get_text()}</p>" for p in raw_chapter.find_all("p") if p.get_text().strip() != ""])
		nextlink = soup.select_one("a.btn-next")["href"]

		self.counter += 1
		return chapter_name, chapter_filename, text, nextlink
	
	def download_all(self):
		book.set_title(self.book_title)
		book.add_author(self.author)

		find_next = True
		self.counter = 1
		while find_next:
			chapter_name, chapter_filename, text, next_url = self.__next_ch()
			ebook_add_chapter(chapter_name, chapter_filename, text)
			if next_url == "#":
				find_next = False
			else:
				self.first_ch = str(next_url)
			# check if request is cached
			if not req.cache.contains(None, None, self.first_ch):
				time.sleep(0.5)

dl = None
if book_url.startswith(SH_HOSTNAME):
	hostname = SH_HOSTNAME
	dl = ScribbleHub(book_url)
else:
	dl = RoyalRoad(book_url)

dl.download_all()

book.toc = tuple(toc_list)
book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())

style = """
	body { color: black; }
	b { font-weight: bold; }
	i { font-style: italic; }
"""
nav_css = epub.EpubItem(
	uid="style_nav",
	file_name="style/nav.css",
	media_type="text/css",
	content=style,
)
book.add_item(nav_css)
book.spine = ["nav", *chapter_obj_list]

output_path = os.path.join(args.output_folder, f"{get_sanizated_string(dl.book_title)}.epub")
epub.write_epub(output_path, book)
print("done", output_path)